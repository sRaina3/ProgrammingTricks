# ProgrammingTricks
Collection of Programming Structures/Algorithms useful to know and understand  

# Java Structures Used:
ArrayList: Quick access and end insertion/deletion, slow insertion/deletion at start and middle.  


# C++ Strucutres Used:
Vector: ArrayList equivlent  
Disjoint Set: Tracks set of elements partitioned into disjoint subsets

# Algorithms Used:
Dynamic Programming - Memoization: Cacheing values for reuse  
Depth First Search (DFS): Method for traversing graphlike structures  
Recursion: Repeting a set of steps until base case to solve repeition problems, dangerous for stack space  
Number Theory: Finding divisors efficiently using squares of divisors  
Union Find: Joins two subsets into single subset (Union) by finding subsets that contain a particular element (Find)  
Binary Search: Find a value in an array in O(logn) time

